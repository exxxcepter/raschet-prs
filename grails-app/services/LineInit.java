import java.util.LinkedList;
import java.util.List;
public class LineInit {
    private List<LineType> lineTypeList;

    /*
    Первый параметр - название типа линии (с добавлением типа тяги, если имя встречается неоднократно)
     */
    public void initLineTypes(){
        lineTypeList = new LinkedList<LineType>();
        lineTypeList.add(new LineType("Провода ДПР, подвешенные с одной стороны путей",
                                      60, 58, 72, 70, 38, 38, 2.0, 2.0, 1.5, 6, 0, 0.1));
        lineTypeList.add(new LineType("Провода ДПР, подвешенные с разных сторон путей",
                                      60, 56, 72, 68, 35, 35, 4.0, 4.0, 2.8, 6, 0, 0.1));
        lineTypeList.add(new LineType("Двухпроводный волновод (электрическая тяга переменного тока 25 кВ)",
                                      60, 56, 72, 68, 38, 38, 1.7, 1.7, 1.5, 6, 0, 0.1));
        lineTypeList.add(new LineType("Однопроводный волновод, подвешенный под проводом ДПР",
                                      60, 54, 72, 66, 38, 38, 2.0, 12.0, 0.0, 6, 0, 0.1));
        lineTypeList.add(new LineType("Пара проводов волновод – провод ДПР",
                                      60, 56, 72, 68, 37, 37, 1.7, 1.7, 1.5, 6, 0, 0.1));
        lineTypeList.add(new LineType("Провод ДПР и питающий провод ПП с одной стороны пути",
                                      60, 58, 72, 70, 37, 37, 2.0, 2.0, 1.5, 6, 0, 4.0));
        lineTypeList.add(new LineType("Два провода трехфазной ВЛС (электрическая тяга постоянного тока)",
                                      58, 52, 70, 64, 40, 40, 2.0, 2.0, 2.0, 6, 6, 0.1));
        lineTypeList.add(new LineType("Двухпроводный волновод (электрическая тяга постоянного тока)",
                                      58, 46, 70, 58, 38, 38, 1.7, 1.7, 1.5, 6, 0, 0.1));
        lineTypeList.add(new LineType("Однопроводный волновод",
                                      58, 46, 70, 58, 30, 38, 2.5, 12.0, 2.1, 6, 6, 0.1));
        lineTypeList.add(new LineType("Цветные цепи ВЛС",
                                      58, 40, 70, 52, 50, 50, 1.5, 1.5, 1.6, 6, 6, 0.1));
        lineTypeList.add(new LineType("Два провода трехфазной ВЛС (автономная тяга)",
                                      38, 50, 47, 59, 50, 50, 2.0, 2.0, 2.0, 3, 6, 0.1));
        lineTypeList.add(new LineType("Цветные цепи ВЛС (автономная тяга)",
                                      38, 30, 47, 39, 50, 50, 1.5, 1.5, 1.6, 3, 6, 0.1));
        lineTypeList.add(new LineType("Однопроводный волновод, подвешенный на самостоятельных опорах",
                                      38, 30, 47, 39, 0, 38, 0.0, 12.0, 2.1, 3, 6, 0.1));
        lineTypeList.add(new LineType("Двухпроводный волновод, подвешенный на самостоятельных опорах",
                                      38, 24, 47, 34, 0, 38, 0.0, 12.0, 1.5, 3, 0, 0.1));
    }

    public List<LineType> getLineTypeList(){
        return lineTypeList;
    }

    public LineType getLineTypeByName(String lineName){
        for (LineType lt : lineTypeList){
            if (lt.getName().equalsIgnoreCase(lineName)) return lt;
        }
        return null;
    }
}
